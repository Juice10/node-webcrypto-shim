const WebCrypto = require('node-webcrypto-ossl').Crypto

const webcrypto = new WebCrypto({
  directory: `${process.env.HOME}/.webcrypto/keys`
})

module.exports = webcrypto
